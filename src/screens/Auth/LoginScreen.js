
import React, { Component } from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity
} from 'react-native';
import colors from '../../utilities/colors';

class LoginScreen extends Component {

  constructor(props){
    super(props);
    this.state = {
      phoneNumber: '',
      password: ''
    };
  }

  _onLogin(){
      console.log('login');
  }
  _onRegister(){
      console.log('register');
  }
  render() {
    return (
      <ScrollView keyboardShouldPersistTaps={'handled'}>
        <Image
          source={require('../../assets/asetLogin.png')}
          style={styles.logo}
        />
        <TextInput
          style={styles.input}
          value={this.state.phoneNumber}
          onChangeText={(phoneNumber) => this.setState({ phoneNumber })}
          placeholder="Masukkan nomor HP"
          keyboardType="numeric"
        />

        <TextInput
          style={styles.input}
          value={this.state.password}
          onChangeText={(password) => this.setState({ password })}
          placeholder="Masukkan password"
          keyboardType="default"
          autoCapitalize='none'
          secureTextEntry
        />
       
        <TouchableOpacity
          style={styles.button}
          onPress={() => this._onLogin()}>
          <Text style={styles.textButton}>LOGIN</Text>
        </TouchableOpacity>

        <Text style={styles.dontHaveAccount}>
          Tidak Punya akun ?
        </Text>

        <TouchableOpacity
          onPress={() => this._onRegister()}>
          <Text style={styles.registerButton}>DAFTAR</Text>
        </TouchableOpacity>
      </ScrollView>
    );
  }
};

const styles = StyleSheet.create({
  logo: {
    width: 160,
    height: 160,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginVertical: 40
  },
  input: {
    height: 40,
    margin: 12,
    borderBottomWidth: 1,
    padding: 10,
  },
  button: {
    alignItems: 'center',
    backgroundColor: colors.primaryColor,
    padding: 12,
    margin: 16,
    marginVertical: 20,
    borderRadius: 8
  },
  textButton: {
    color: colors.whiteColor,
    fontWeight: 'bold' 
  },
  dontHaveAccount: {
    textAlign: 'center'
  },
  registerButton: {
    textAlign: 'center',
    color: colors.primaryColor,
    marginVertical: 16
  }

});

export default LoginScreen;
